class Course {
  String name;
  String description;
  int price;

  Course({required this.name, required this.description, required this.price}) ;
}
