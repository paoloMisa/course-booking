import 'package:flutter/material.dart';
// Import course.dart to have access to Course class
import 'course.dart';

void main() {
  runApp(
    MaterialApp(
      title: 'Navigation Basics',
      initialRoute: '/',
      routes: {
        '/': (context) => const CourseList(),
        '/course-detail': (context) => const CourseDetail(),
        '/about-us': (context) => const ContactUsPage(),
      },
    ),
  );
}

// "Course List" - This will serve as our FIRST screen
class CourseList extends StatelessWidget {
  const CourseList({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    void learnMoreButton(title, description) {
      // Navigate to the "Course Detail" screen using a named route.
      // Add parameters so we can pass the course name (title) and description
      Navigator.pushNamed(context, '/course-detail',
          arguments: CourseArguments(title, description));
    }

    void contactUsAction() {
      // Navigate to the "Contact Us" screen using a named route.
      const String email = 'helpdesk@zuitt.co';
      const String mobile = '09171668714 (globe); 09328688713 (smart)';

      // Pass the email and mobile information
      Navigator.pushNamed(context, '/about-us',
          arguments: ContactUsArguments(email, mobile));
    }

    // Creating a list of course objects (Course class)
    List<Course> courses = [
      Course(
          name: 'Flutter',
          description: 'Learn everything about Flutter',
          price: 15000),
      Course(
          name: 'JavaScript 101',
          description: 'Basics of Javascript',
          price: 5000),
      Course(
          name: 'Django Mastery',
          description: 'Be a Django Master',
          price: 25000),
    ];

    // card Template
    Widget cardTemplate(course) {
      return Card(
        margin: const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 0),
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Text(course.name,
                  style: TextStyle(fontSize: 18.0, color: Colors.grey[600])),
              const SizedBox(
                height: 6.0,
              ),
              Text(
                course.description,
                style: TextStyle(fontSize: 14.0, color: Colors.grey[800]),
              ),
              Text('price: ${course.price}',
                  style: TextStyle(fontSize: 14.0, color: Colors.grey[800])),
              ElevatedButton(
                  child: const Text('Learn More'),
                  onPressed: () =>
                      learnMoreButton(course.name, course.description))
            ],
          ),
        ),
      );
    }

    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
          centerTitle: true,
          title: Image.asset('assets/images/zuittlogo.png'),
          actions: <Widget>[
            IconButton(
              icon: const Icon(Icons.info_outline),
              onPressed: contactUsAction,
            ),
          ]),
      body: Column(
        children: courses.map((course) => cardTemplate(course)).toList(),
      ),
    ));
  }
}

// "Course Detail" - This will serve as our Second screen
class CourseDetail extends StatelessWidget {
  const CourseDetail({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    // Extract the arguments from the current ModalRoute
    // settings and cast them as ScreenArguments.
    final args = ModalRoute.of(context)!.settings.arguments as CourseArguments;

    return MaterialApp(
        home: Scaffold(
            appBar: AppBar(
              centerTitle: true,
              title: Image.asset('assets/images/zuittlogo.png'),
              leading: IconButton(
                icon: const Icon(Icons.arrow_back),
                onPressed: () {
                  // Navigate back to the first screen by popping the current route
                  // off the stack.
                  Navigator.pop(context);
                },
              ),
            ),
            body: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Text(
                    args.title,
                    style: Theme.of(context).textTheme.headline4,
                  ),
                  Text(args.description),
                ],
              ),
            )));
  }
}

// "Contact Us" - This will serve as our THIRD screen
class ContactUsPage extends StatelessWidget {
  const ContactUsPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    // Extract the arguments from the current ModalRoute
    // settings and cast them as ScreenArguments.
    final args =
        ModalRoute.of(context)!.settings.arguments as ContactUsArguments;

    final String emailInfo = 'Email: ${args.email}';
    final String mobileInfo = 'Mobile: ${args.mobile}';

    return MaterialApp(
        home: Scaffold(
            appBar: AppBar(
              centerTitle: true,
              title: Image.asset('assets/images/zuittlogo.png'),
              leading: IconButton(
                icon: const Icon(Icons.arrow_back),
                onPressed: () {
                  // Navigate back to the first screen by popping the current route
                  // off the stack.
                  Navigator.pop(context);
                },
              ),
            ),
            body: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Text(
                    'Contact Us',
                    style: Theme.of(context).textTheme.headline4,
                  ),
                  Text(emailInfo),
                  Text(mobileInfo),
                ],
              ),
            )));
  }
}

// Use this class to pass data of courses to
// "CourseDetail" screen
class CourseArguments {
  final String title;
  final String description;

  CourseArguments(this.title, this.description);
}

// Use this class to pass data of courses to
// "Contact Us" screen
class ContactUsArguments {
  final String email;
  final String mobile;

  ContactUsArguments(this.email, this.mobile);
}
